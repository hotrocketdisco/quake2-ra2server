# Quake 2 Rocket Arena Server

This docker container runs a Quake 2 Linux server with the Rocket Arena 2 (arena) mod.

## About this container

The container is based on an Alpine Linux image with GLIBC support (vimalathithen/alpine-glibc-x86) to run the id legacy Quake2 server.

## Ideas

* Build this based on https://github.com/tnielsen2/q2/blob/master/Dockerfile

## Roadmap

* Support images for x64 and ARM (Raspberry) platforms (Alpine Linux supports both, but glibc needs to be setup for both platforms)
* Use docker multistage builds (https://docs.docker.com/develop/develop-images/multistage-build/), to create a glibc enabled apline container as base, which serves for quake servers
* Split container hierarchy into
    * Base image (with glibc)
    * basic Quake server image
    * Images with added mod flavours (ra2)
* Exclude required *.pk3 files from the repository and add them via path variable to the image build process or later as mounted volume
* Build images for supported platforms automatically via Gitlab CI/CD

## Quake 2 *.pk2 files

This repository does not include the `pak0.pk2` file which is required by the server as it cannot not be distributed. To add this file to your image build, just add it into the `paks/baseq2` folder. You can also use the build argument `Q2_PAK_FILES` to specify a location containing your `pak0.pk2` file. To keep your container image small, it is advised to mount those folders later when running the container.

## Build argumnets

* IMAGE_TAG
* IMAGE_BASE
* Q2\_PAK\_FILES <span style="color:blue">*./paks/baseq2*</span>
* Q2\_GAME\_FILES <span style="color:blue">*./game*</span>
* Q2\_GAME\_FOLDER <span style="color:blue">*/quake2*</span>
* RA2\_CONFIG\_FILES <span style="color:blue">*./config*</span>
* RA2\_PAK\_FILES <span style="color:blue">*./paks/arena*</span>
* ENTRYPOINT_SCRIPT <span style="color:blue">*./entrypoint.sh*</span>

## Supported environment variables

You can run the container image with custom Quake 2 / Rocket Arena 2 settings by providing the respective environment variables

* Q2\_GAME\_FOLDER <span style="color:blue">*/quake2*</span>
* Q2\_SERVER\_PORT <span style="color:blue">*27960*</span>
* RA2\_INITIAL\_MAP <span style="color:blue">*ra2map1*</span>
* RA2\_SERVER\_MODE <span style="color:blue">*1*</span>
* RA2\_SERVER\_PURE <span style="color:blue">*1*</span>
* RA2\_SERVER\_HOSTNAME <span style="color:blue">*Rocket Arena 2 Server*</span>
* RA2\_SERVER\_MODT <span style="color:blue">*Welcome to Rocket Arena 2...*</span>
* RA2\_MAX\_CLIENTS <span style="color:blue">*16*</span>
* RA2\_TIME\_LIMIT <span style="color:blue">*30*</span>
* RA2\_ADMIN\_PASSWORD <span style="color:blue">*123456*</span>
* RA2\_RCON\_PASSWORD <span style="color:blue">*123456*</span>
* RA2\_RCON\_ENABLED <span style="color:blue">*false*</span>
* RA2\_PRIVATE\_PASSWORD <span style="color:blue">*123456*</span>
* RA2\_PRIVATE\_ENABLED <span style="color:blue">*false*</span>
* RA2\_PRIVATE\_SLOTS <span style="color:blue">*0*</span>

## How to build your container?

Build a default Rocket Arena container with your own tag:

    docker build -t {YOUR_IMAGE_TAG} .
    
The server requires at least the `pak0.pk2` file which must not be distributed. This file can be later mounted into your container or pre-packed into the image. To add your own `*.pk2` files, copy your files into the default `./paks` folder or use the following build command to specify a path:

    docker build -t {YOUR_IMAGE_TAG} --build-arg RA2_PAK_FILES={CUSTOM_PATH} .
    
## How to run the server?

Run a default Rocket Arena server at port 27960:

    docker run -it --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp {YOUR_IMAGE_TAG}
    
To change your server settings on running the container simply provide environment variables like in the following example or use an `.env file`.
    
    docker run -i -t --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp -e RA3_SERVER_HOSTNAME='Hot Rocket Disco' {YOUR_IMAGE_TAG}
    
You can also directly start a server by passing the respective server parameters to `q3ded`. In this case you simply use the image as a `q3ded` executable.

    docker run -i -t --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp {YOUR_IMAGE_TAG} {ARG1} {ARG2} {ARG3} ...